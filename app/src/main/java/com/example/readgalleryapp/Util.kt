package com.example.readgalleryapp

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DimenRes
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.readgalleryapp.data.DisplayData

fun Activity.isHasPermission(vararg permissions: String): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        permissions.all { singlePermission ->
            applicationContext.checkSelfPermission(singlePermission) == PackageManager.PERMISSION_GRANTED
        }
    } else true
}

fun Activity.askPermission(requestCode: Int, vararg permissions: String) =
    ActivityCompat.requestPermissions(this, permissions, requestCode)

fun Fragment.isHasPermission(vararg permissions: String): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        permissions.all { singlePermission ->
            requireContext().checkSelfPermission(singlePermission) == PackageManager.PERMISSION_GRANTED
        }
    } else true
}

fun Fragment.askPermission(requestCode: Int, vararg permissions: String) {
    requestPermissions(permissions, requestCode)
}

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun Context.getDisplayData(@DimenRes id: Int): DisplayData {
    val dm = resources.displayMetrics
    val screenWidth = dm.widthPixels
    val columnWidth = resources.getDimensionPixelSize(id)
    return DisplayData(screenWidth, (screenWidth / columnWidth))
}