package com.example.readgalleryapp.di

import android.content.ContentResolver
import com.example.readgalleryapp.data.ImagesRepository
import com.example.readgalleryapp.data.ImagesRepositoryImpl
import com.example.readgalleryapp.data.contacts.ContactsRepository
import com.example.readgalleryapp.data.contacts.ContactsRepositoryImpl
import com.example.readgalleryapp.data.contacts.dataprovider.ContactsDataProvider
import com.example.readgalleryapp.data.contacts.dataprovider.EmailsDataProvider
import com.example.readgalleryapp.data.contacts.dataprovider.NumbersDataProvider
import com.example.readgalleryapp.ui.contacts.ContactsVM
import com.example.readgalleryapp.ui.images.ImagesViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import kotlin.math.sin

val readImagesModule = module {
    single<ContentResolver> { androidApplication().contentResolver }
    single<ImagesRepository> { ImagesRepositoryImpl(get()) }
    viewModel { ImagesViewModel(get()) }
}

val readContactsModule = module {
    single { ContactsDataProvider(get()) }
    single { NumbersDataProvider(get()) }
    single { EmailsDataProvider(get()) }
    single<ContactsRepository> { ContactsRepositoryImpl(get(), get(), get()) }
    viewModel { ContactsVM(get()) }
}