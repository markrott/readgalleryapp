package com.example.readgalleryapp

import android.app.Application
import com.example.readgalleryapp.di.readContactsModule
import com.example.readgalleryapp.di.readImagesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApp)
            modules(readImagesModule, readContactsModule)
        }
    }
}