package com.example.readgalleryapp.data

interface ImagesRepository {

    suspend fun fetchImagesFromGallery() : List<ImageItem>
}