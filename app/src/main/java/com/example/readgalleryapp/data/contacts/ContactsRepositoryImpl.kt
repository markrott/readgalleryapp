package com.example.readgalleryapp.data.contacts

import com.example.readgalleryapp.data.contacts.dataprovider.ContactsDataProvider
import com.example.readgalleryapp.data.contacts.dataprovider.EmailsDataProvider
import com.example.readgalleryapp.data.contacts.dataprovider.NumbersDataProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContactsRepositoryImpl(
    private val contactsDataProvider: ContactsDataProvider,
    private val numbersDataProvider: NumbersDataProvider,
    private val emailsDataProvider: EmailsDataProvider
) : ContactsRepository {

    override suspend fun getContacts(): List<Contact> {
        return withContext(Dispatchers.IO) {
            contactsDataProvider.retrieveContacts()
        }
    }

    override suspend fun getContactNumbers(): HashMap<String, HashSet<String>> {
        return withContext(Dispatchers.IO) {
            numbersDataProvider.retrievePhoneNumbers()
        }
    }

    override suspend fun getContactEmails(): HashMap<String, ArrayList<String>> {
        return withContext(Dispatchers.IO) {
            emailsDataProvider.retrieveEmails()
        }
    }
}