package com.example.readgalleryapp.data.contacts.dataprovider

import android.content.ContentResolver
import android.content.ContentUris
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import com.example.readgalleryapp.data.contacts.Contact

class ContactsDataProvider(private val contentResolver: ContentResolver) {

    fun retrieveContacts() : MutableList<Contact> {
        val contacts = mutableListOf<Contact>()
        val cursor = getPhoneContactsCursor()
        fillContactList(cursor, contacts)
        cursor?.close()
        return contacts
    }

    private fun fillContactList(cursor: Cursor?, contactList: MutableList<Contact>) {
        if (cursor != null && cursor.count > 0) {
            val idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID)
            val nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            while (cursor.moveToNext()) {
                val id = cursor.getString(idIndex)
                val name = cursor.getString(nameIndex)
                val person: Uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id.toLong())
                val avatarPath = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)
                name?.let {
                    contactList.add(Contact(id, name, avatarUri = avatarPath))
                }
            }
        }
    }

    private fun getPhoneContactsCursor(): Cursor? {
        return contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )
    }
}