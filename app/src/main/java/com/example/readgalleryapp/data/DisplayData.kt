package com.example.readgalleryapp.data

data class DisplayData(val screenWidth: Int, val spanCount: Int){
    fun getItemWidth() : Int = screenWidth / spanCount
}