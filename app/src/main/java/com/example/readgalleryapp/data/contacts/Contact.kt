package com.example.readgalleryapp.data.contacts

import android.net.Uri

data class Contact(
    val id: String,
    val name: String,
    var numbers: HashSet<String> = hashSetOf(),
    var emails: List<String> = emptyList(),
    val avatarUri: Uri
)