package com.example.readgalleryapp.data

import android.content.ContentResolver
import android.content.ContentUris
import android.provider.MediaStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ImagesRepositoryImpl(private val contentResolver: ContentResolver) : ImagesRepository {

    override suspend fun fetchImagesFromGallery(): List<ImageItem> {
        val pathList = mutableListOf<ImageItem>()
        return withContext(Dispatchers.IO) {
            // INTERNAL_CONTENT_URI --> fetch the images which are created by my application
            // EXTERNAL_CONTENT_URI --> get all images that are created by any application
            val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val imageSortOrder = "${MediaStore.Images.Media.DATE_ADDED} DESC"

            val imageProjection = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATE_ADDED,
            )

            val cursor = contentResolver.query(
                uri,
                imageProjection,
                null,
                null,
                imageSortOrder
            )

            cursor?.use {
                val idColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                val nameColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                val sizeColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)
                val dateColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED)

                while (it.moveToNext()) {
                    val id = it.getLong(idColumn)
                    val contentUri = ContentUris.withAppendedId(uri, id)
                    val name = it.getString(nameColumn)
                    val size = it.getString(sizeColumn)
                    val date = it.getString(dateColumn)

                    pathList.add(ImageItem(id, contentUri, name, size, date))
                }
            }
            pathList
        }
    }
}