package com.example.readgalleryapp.data

import android.net.Uri

data class ImageItem(val id: Long, val path: Uri, val name: String, val size: String, val date: String)