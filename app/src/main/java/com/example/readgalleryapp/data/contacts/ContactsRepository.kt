package com.example.readgalleryapp.data.contacts

interface ContactsRepository {
    suspend fun getContacts(): List<Contact>
    suspend fun getContactNumbers(): HashMap<String, HashSet<String>>
    suspend fun getContactEmails(): HashMap<String, ArrayList<String>>
}