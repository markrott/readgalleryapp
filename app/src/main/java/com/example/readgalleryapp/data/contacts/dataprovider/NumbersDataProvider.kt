package com.example.readgalleryapp.data.contacts.dataprovider

import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract

class NumbersDataProvider(private val contentResolver: ContentResolver) {

    fun retrievePhoneNumbers(): HashMap<String, HashSet<String>> {
        val numberMap = HashMap<String, HashSet<String>>()
        val cursor = getNumberCursor()
        fillNumbersMap(cursor, numberMap)
        cursor?.close()
        return numberMap
    }

    private fun fillNumbersMap(cursor: Cursor?, numberMap: HashMap<String, HashSet<String>>) {
        if (cursor != null && cursor.count > 0) {
            val idIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
            val numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            while (cursor.moveToNext()) {
                val contactId = cursor.getString(idIndex)
                var number: String = cursor.getString(numberIndex)
                if (number.isNotEmpty()) {
                    number = number
                        .replace(" ", "")
                        .replace("-", "")
                        .replace("(", "")
                        .replace(")", "")
                        .replace("+", "")
                    if (numberMap.containsKey(contactId)) {
                        numberMap[contactId]?.add(number)
                    } else numberMap[contactId] = hashSetOf(number)
                }
            }
        }
    }

    private fun getNumberCursor(): Cursor? {
        return contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )
    }
}