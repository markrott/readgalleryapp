package com.example.readgalleryapp.data.contacts.dataprovider

import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract

class EmailsDataProvider(private val contentResolver: ContentResolver) {

    fun retrieveEmails() : HashMap<String, ArrayList<String>> {
        val emailMap = HashMap<String, ArrayList<String>>()
        val cursor = getEmailCursor()
        fillEmailMap(cursor, emailMap)
        cursor?.close()
        return emailMap
    }

    private fun fillEmailMap(cursor: Cursor?, emailMap: HashMap<String, ArrayList<String>>) {
        if (cursor != null && cursor.count > 0) {
            val idIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)
            val emailIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)
            while (cursor.moveToNext()) {
                val contactId = cursor.getString(idIndex)
                val email = cursor.getString(emailIndex)
                if (emailMap.containsKey(contactId)) {
                    emailMap[contactId]?.add(email)
                } else emailMap[contactId] = arrayListOf(email)
            }
        }
    }

    private fun getEmailCursor(): Cursor? {
        return contentResolver.query(
            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
            null,
            null,
            null,
            null
        )
    }
}