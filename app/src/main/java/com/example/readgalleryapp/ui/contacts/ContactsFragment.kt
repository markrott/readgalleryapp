package com.example.readgalleryapp.ui.contacts

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.readgalleryapp.CONTACTS_REQUEST_CODE
import com.example.readgalleryapp.R
import com.example.readgalleryapp.askPermission
import com.example.readgalleryapp.isHasPermission
import kotlinx.android.synthetic.main.frg_contacts.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactsFragment : Fragment() {

    private val contactsVM: ContactsVM by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_contacts, container, false)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CONTACTS_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            contactsVM.fetchContactsFromTelephone()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        checkPermission()
        subscribeToProgressBar()
        subscribeToContacts()
    }

    private fun checkPermission() {
        val permissions = arrayOf(Manifest.permission.READ_CONTACTS)
        if (isHasPermission(*permissions)) {
            contactsVM.fetchContactsFromTelephone()
        } else askPermission(CONTACTS_REQUEST_CODE, *permissions)
    }

    private fun subscribeToContacts() {
        contactsVM.contactsLD.observe(viewLifecycleOwner) {
            rcv_contacts.adapter = ContactsAdapter(it)
        }
    }

    private fun subscribeToProgressBar() {
        contactsVM.progressLD.observe(viewLifecycleOwner) {
            progress_bar.isVisible = it
        }
    }
}