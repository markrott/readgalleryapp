package com.example.readgalleryapp.ui.images

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.readgalleryapp.data.ImageItem
import com.example.readgalleryapp.data.ImagesRepository
import kotlinx.coroutines.launch

class ImagesViewModel(private val imageRepo: ImagesRepository) : ViewModel() {

    private val _imagePathList: MutableLiveData<List<ImageItem>> = MutableLiveData()
    private val _loadState: MutableLiveData<Boolean> = MutableLiveData()

    val imagePathList: LiveData<List<ImageItem>> get() = _imagePathList
    val loadState: LiveData<Boolean> get() = _loadState

    fun fetchImagesFromGallery() {
        if (_imagePathList.value.isNullOrEmpty()) {
            viewModelScope.launch {
                try {
                    _loadState.value = true
                    val pathList = imageRepo.fetchImagesFromGallery()
                    _imagePathList.value = pathList
                } finally {
                    _loadState.value = false
                }
            }
        }
    }
}