package com.example.readgalleryapp.ui.contacts

import android.view.View
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.readgalleryapp.R
import com.example.readgalleryapp.data.contacts.Contact
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(contact: Contact) {
        itemView.tv_name.text = contact.name
        itemView.tv_phone_number.text = contact.numbers.toString()
        itemView.tv_email.isGone = contact.emails.isEmpty()
        itemView.tv_email.text = contact.emails.toString()

        Glide
            .with(itemView.context)
            .load(contact.avatarUri)
            .error(R.drawable.ic_person)
            .into(itemView.iv_avatar)
    }
}