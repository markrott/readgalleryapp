package com.example.readgalleryapp.ui.images

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.readgalleryapp.R
import com.example.readgalleryapp.data.ImageItem
import com.example.readgalleryapp.getDisplayData
import com.example.readgalleryapp.inflate
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdapter : RecyclerView.Adapter<ImageViewHolder>() {

    private val items = mutableListOf<ImageItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = parent.inflate(R.layout.item_image)
        prepareView(view)
        return ImageViewHolder(view)
    }

    private fun prepareView(view: View) {
        val ctx = view.context
        val displayData = ctx.getDisplayData(R.dimen.column_width)
        val itemWidth = displayData.getItemWidth()
        val params = LinearLayout.LayoutParams(itemWidth, itemWidth)
        view.frm_holder.layoutParams = params
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = items.size

    fun setupItems(data: List<ImageItem>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }
}