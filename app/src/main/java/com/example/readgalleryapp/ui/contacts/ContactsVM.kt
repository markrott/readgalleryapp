package com.example.readgalleryapp.ui.contacts

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.readgalleryapp.APP_TAG
import com.example.readgalleryapp.data.contacts.Contact
import com.example.readgalleryapp.data.contacts.ContactsRepository
import kotlinx.coroutines.*

class ContactsVM(private val repo: ContactsRepository) : ViewModel() {

    private val _progressMLD: MutableLiveData<Boolean> = MutableLiveData()
    private val _contactsMLD: MutableLiveData<List<Contact>> = MutableLiveData()

    val progressLD: LiveData<Boolean> = _progressMLD
    val contactsLD: LiveData<List<Contact>> = _contactsMLD

    fun fetchContactsFromTelephone() {
        if (_contactsMLD.value.isNullOrEmpty()) {
            _progressMLD.value = true
            viewModelScope.launch(exceptionHandler) {

                val contactsAsync = async { repo.getContacts() }
                val numbersAsync = async { repo.getContactNumbers() }
                val emailsAsync = async { repo.getContactEmails() }

                val contacts = contactsAsync.await()
                val numbers = numbersAsync.await()
                val emails = emailsAsync.await()

                contacts.forEach {
                    fillNumbers(numbers, it)
                    fillEmails(emails, it)
                }

                _contactsMLD.value = contacts.filter { it.numbers.isNotEmpty() }
                _progressMLD.value = false
            }
        }
    }

    private fun fillNumbers(numbers: HashMap<String, HashSet<String>>, contact: Contact) {
        numbers[contact.id]?.let {
            if (it.isNotEmpty()) {
                contact.numbers = it
            }
        }
    }

    private fun fillEmails(emails: HashMap<String, ArrayList<String>>, contact: Contact) {
        emails[contact.id]?.let {
            if (emails.isNotEmpty()) {
                contact.emails = it
            }
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        Log.e(APP_TAG, "Fetch contacts from telephone error: $exception")
    }
}