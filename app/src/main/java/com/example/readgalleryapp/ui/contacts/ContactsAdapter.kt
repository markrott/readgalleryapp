package com.example.readgalleryapp.ui.contacts

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readgalleryapp.R
import com.example.readgalleryapp.data.contacts.Contact
import com.example.readgalleryapp.inflate

class ContactsAdapter(private val items: List<Contact>) : RecyclerView.Adapter<ContactsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder =
        ContactsViewHolder(parent.inflate(R.layout.item_contact))

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size
}