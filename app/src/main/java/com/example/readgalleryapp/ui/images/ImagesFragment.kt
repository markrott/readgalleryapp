package com.example.readgalleryapp.ui.images

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.readgalleryapp.R
import com.example.readgalleryapp.askPermission
import com.example.readgalleryapp.getDisplayData
import com.example.readgalleryapp.isHasPermission
import kotlinx.android.synthetic.main.frg_images_gallery.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ImagesFragment : Fragment() {

    private val imageRequestCode = 1221
    private val imagesViewModel: ImagesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_images_gallery, container, false)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == imageRequestCode &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            fetchImages()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToLoadState()
        subscribeToImagesPath()
        checkPermission()
    }

    private fun subscribeToLoadState() {
        imagesViewModel.loadState.observe(viewLifecycleOwner) {
            progress_bar.isVisible = it
        }
    }

    private fun subscribeToImagesPath() {
        imagesViewModel.imagePathList.observe(viewLifecycleOwner) { pathList ->
            val displayData = requireContext().getDisplayData(R.dimen.column_width)
            rcv_images.layoutManager = GridLayoutManager(requireContext(), displayData.spanCount)
            rcv_images.setHasFixedSize(true)
            val adapter = ImageAdapter()
            rcv_images.adapter = adapter
            adapter.setupItems(pathList)
        }
    }

    private fun checkPermission() {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (isHasPermission(*permissions)) {
            fetchImages()
        } else askPermission(imageRequestCode, *permissions)
    }

    private fun fetchImages() {
        imagesViewModel.fetchImagesFromGallery()
    }
}