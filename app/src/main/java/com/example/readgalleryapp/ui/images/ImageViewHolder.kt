package com.example.readgalleryapp.ui.images

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.readgalleryapp.data.ImageItem
import kotlinx.android.synthetic.main.item_image.view.*

class ImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(model: ImageItem) {
        Glide.with(itemView.context).load(model.path).into(itemView.iv_image)
    }
}