**This application implements:**
- getting data from the gallery 
- getting contact information

---

**Fetching contacts using kotlin coroutines**

- method `getContacts()` -> fetch all contacts and add it to List.
- method `getContactNumbers()` -> fetch all contact Numbers from Data table and store it in HashMap with CONTACT_ID as key.
- method `getContactEmails()` -> fetch all email IDs from Data table and store it in HashMap with CONTACT_ID as key.

---

**Fetching images using kotlin coroutines**

- method `fetchImagesFromGallery()` -> fetch all images from gallery

---

**Link to video:** https://youtu.be/tAMMllta8us

---

![Gallery example](/app/src/main/assets/images.jpg)
![Contacts example](/app/src/main/assets/contacts.jpg)